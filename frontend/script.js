// api url
const api_url =
	"http://localhost:8080/finalproject_war_exploded/frontend";

// Defining async function
async function getapi(url) {

	// Storing response
	const response = await fetch(url);

	// Storing data in form of JSON
	var data = await response.json();
	console.log(data);
	if (response) {
		hideloader();
	}
	show(data);
}
// Calling that async function
getapi(api_url);

// Function to hide the loader
function hideloader() {
	document.getElementById('loading').style.display = 'none';
}
// Function to define innerHTML for HTML table
function show(data) {
	let tab =
		`<tr>
	<!--	<th>Id</th> -->
		<th>Name</th>
		<th>Street</th>
		<th>Phone</th>
	<!--	<th>Latitude</th> -->
	<!--	<th>Longitude</th> -->
	<!--	<th>Manager ID</th> -->
		<th>Manager Name</th>
		<th>Manager Last Name</th>
		<th>Manager Email</th>
		<th>Manager Phone</th>
		</tr>`;

	// Loop to access all rows
	for (let r of data.list) {
		tab += `<tr>
	<!-- <td>${r.id} </td> -->
	<td>${r.name} </td>
	<td>${r.street} </td>
	<td>${r.phone} </td>
	<!-- <td>${r.latitude}</td> -->
	<!-- <td>${r.longitude} </td> -->
	<!-- <td>${((r || {}).manager || {}).id} </td> -->
	<td>${((r || {}).manager || {}).name} </td>
	<td>${((r || {}).manager || {}).lastname} </td>
	<td>${((r || {}).manager || {}).email} </td>
	<td>${((r || {}).manager || {}).mobile} </td>
</tr>`;
var marker = L.marker([r.latitude, r.longitude]).addTo(map);
marker.bindPopup("<h4>" + r.name + "</h4><h6>Street: " + r.street + " - Phone:"+ r.phone + "</h6>",{closeOnClick: false, autoClose: false}).openPopup();
	}

	// Setting innerHTML as tab variable
	document.getElementById("branches").innerHTML = tab;
}

document.body.innerHTML += '<div id="map"></div>';


var map = L.map('map').setView([-32.55832, -55.8117], 7);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYWxlZmluaXIiLCJhIjoiY2sxanF5enhxMG51azNtb2QxY2Mwa215YiJ9.vr6zgo6_9GDWqYoL8xh2lg'
}).addTo(map);

var marker = L.marker([-32.55832, -55.8117]).addTo(map);
//marker.bindPopup("<b>Hello world!</b><br>I am a popup.").openPopup();

var circle = L.circle([-32.55832, -55.8117], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(map);
