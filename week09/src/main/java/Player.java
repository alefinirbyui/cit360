import javax.persistence.*;

@Entity
@Table(name = "player")
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "playerId")
    private int id;
    @Column(name = "playerName")
    private String name;
    @Column(name = "playerAge")
    private int age;
    @Column(name = "playerYearsP")
    private int yearsPlaying;

    public Player(){
    };


    public Player( String name, int age, int yearsPlaying){
        this.id = id;
        this.name = name;
        this.age = age;
        this.yearsPlaying = yearsPlaying;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public int getYearsPlaying() {
        return yearsPlaying;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYearsPlaying(int yearsPlaying) {
        this.yearsPlaying = yearsPlaying;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", yearsPlaying=" + yearsPlaying +
                '}';
    }
}
