import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class tournamentManager {
	protected SessionFactory sessionFactory;
	//setup connection to mysql
	protected void setup() {
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.configure() //settings from hibernate.cfg.xml
				.build();
		try {
			sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception ex) {
			StandardServiceRegistryBuilder.destroy(registry);
			System.out.println("The connection could not be made..");
		}
	}

	protected void exit() {
		try{
			sessionFactory.close();
		}catch (Exception e) {
			System.out.println("The Session cannot be closed");
			e.printStackTrace();
		}
	}

	//create a player in database
	protected void addPlayer(Player p1) {
		try(Session session = sessionFactory.openSession()){
			session.beginTransaction();
			//save player
			session.save(p1);
			session.getTransaction().commit();
			session.close();
		}catch (Exception e) {
			System.out.println("The Player cannot be added");
			e.printStackTrace();
		}
	}

	//create a team in database
	protected void addTeam(Team t1) {
		try(Session session = sessionFactory.openSession()){
			session.beginTransaction();
			//save team
			session.save(t1);

			session.getTransaction().commit();
			session.close();
		}catch (Exception e) {
			System.out.println("The Team cannot be added");
			e.printStackTrace();
		}
	}

	//get a player and print it
	protected void readPlayer(int playerId) {
		try(Session session = sessionFactory.openSession()){
			Player p1 = session.get(Player.class, playerId);
			if(p1 != null) {
				System.out.println(p1.toString());
			}
			session.close();
		}catch (Exception e) {
			System.out.println("Player information could not be obtained.");
			e.printStackTrace();
		}
	}

	//get a team and print it
	protected void readTeam(int teamId) {
		try(Session session = sessionFactory.openSession()){
			Team t1 = session.get(Team.class, teamId);
			if(t1 != null) {
				System.out.println(t1.toString());
			}
			session.close();
		}catch (Exception e) {
			System.out.println("Team information could not be obtained.");
			e.printStackTrace();
		}
	}

	//update a Player
	protected void updatePlayer(Player p1) {
		try(Session session = sessionFactory.openSession()){
			session.beginTransaction();
			//update player
			session.update(p1);
			session.getTransaction().commit();
			session.close();
		}catch (Exception e) {
			System.out.println("Player information could not be updated.");
			e.printStackTrace();
		}
	}
	//update a Player
	protected void updateTeam(Team t1) {
		try(Session session = sessionFactory.openSession()){
			session.beginTransaction();
			//update team
			session.update(t1);
			session.getTransaction().commit();
			session.close();
		}catch (Exception e) {
			System.out.println("Team information could not be updated.");
			e.printStackTrace();
		}
	}

	//delete a player
	protected void deletePlayer( Player p1) {
		try(Session session = sessionFactory.openSession()){
			session.beginTransaction();
			//delete player
			session.delete(p1);
			session.getTransaction().commit();
			session.close();
		}catch (Exception e) {
			System.out.println("Player information could not be deleted.");
			e.printStackTrace();
		}
	}
	//delete a player
	protected void deleteTeam( Team t1) {
		try(Session session = sessionFactory.openSession()){
			session.beginTransaction();
			//delete team
			session.delete(t1);
			session.getTransaction().commit();
			session.close();
		}catch (Exception e) {
			System.out.println("Team information could not be deleted.");
			e.printStackTrace();
		}

	}

	//print team table
	protected void printAllTeams(){
		try(Session session = sessionFactory.openSession()){
			session.beginTransaction();
			List< Team > teams = session.createQuery("from Team", Team.class).list();
			teams.forEach(t -> System.out.println(t.toString()));
			session.getTransaction().commit();
			session.close();
		}catch (Exception e) {
			System.out.println("Teams information could not be obtained.");
			e.printStackTrace();
		}
	}
	//print player table
	protected void printAllPlayers(){
		try(Session session = sessionFactory.openSession()){
			session.beginTransaction();
			List< Player > players = session.createQuery("from Player", Player.class).list();
			players.forEach(p -> System.out.println(p.toString()));
			session.getTransaction().commit();
			session.close();
		}catch (Exception e) {
			System.out.println("Players information could not be obtained.");
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		tournamentManager manager = new tournamentManager();
		manager.setup();

		//create 2 team and add this
		Team t1 = new Team("red");
		Team t2 = new Team("blue");
		//add teams to table
		manager.addTeam(t1);
		manager.addTeam(t2);

		//create 2 players
		Player p1 =new Player("Rick",22,10);
		Player p2 =new Player("Jon",29,5);
		//add teams to table
		manager.addPlayer(p1);
		manager.addPlayer(p2);

		//print 1 team
		manager.readTeam(t1.getId());

		//print 1 player
		manager.readPlayer(p2.getId());
		manager.readPlayer(200);

		//update player
		Player p3 =new Player("Jon Doe",29,5);
		//set id of the player to modify
		p3.setId(p2.getId());
		manager.updatePlayer(p3);

		//print modified player
		manager.readPlayer(p2.getId());

		//print all teams
		manager.printAllTeams();

		//print all players
		manager.printAllPlayers();

		//delete team1
		manager.deleteTeam(t1);



		//exit and delete all tables
		//manager.exit();
	}

}
