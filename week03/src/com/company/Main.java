/*
*2) Use the following requirements when writing your program:
* -a Create a program that gathers input of two numbers from the keyboard in its main method and then calls the method below to calculate.
* -b Write a method that divides the two numbers together (num1/num2) and returns the result. The method should include a throws clause for the most specific exception possible if the user enters zero for num2.
* -c In your main method, use exception handling to catch the most specific exception possible. Display a descriptive message in the catch to tell the user what the problem is. Give the user a chance to retry the data entry. Display a message in the final statement.
* -d Run and show the output when there is no exception and when a zero in the denominator is used.
* */


package com.company;

import java.util.Scanner;

public class Main {
    // Division Method
    static void doDivision(float num1, float num2){
        //make a zero variable
        Float zero = new Float(0);

        //check if num2 is zero
        if (num2 == zero) {
            throw new ArithmeticException("Division by 0");
        }
        else {
            System.out.println("Result of division is: " + num1/num2);
        }
    }

    //check number Method
    static boolean isNumber(String strNumber){
        try {
            Float.parseFloat(strNumber);
            return true;
        }catch (NumberFormatException e){
            System.out.println("Sorry, Invalid number");
            return false;
        }
    }

    //main method
    public static void main(String[] args) {
        Scanner input1 = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);

        System.out.println("###Welcome to the division calculator -- EXCEPTIONS###");

        //validation number1
        String num1;
        do {
            System.out.println("Enter the FIRST number");
            num1 = input1.nextLine();
        } while (!isNumber(num1));

        boolean divZero = false;

        do {
            //validation number2
            String num2;
            do {
                System.out.println("Enter the SECOND number");
                num2 = input2.nextLine();
            } while (!isNumber(num2));

            try {
                //both are numbers so you can do division
                doDivision(Float.parseFloat(num1), Float.parseFloat(num2));
                divZero = false;
            }catch (Exception e){
                System.out.println("The Number can not be 0");
                divZero = true;
            }

        }while(divZero);

    }
}
