/*
*2) Use the following requirements when writing your program:
* -e Rewrite the data collection so that it uses data validation to ensure an error does not occur. If the user does type a zero in the denominator, display a message and give the user a chance to retry.
* -f Run and show the output when there is no problem with data entry and when a zero is used in the denominator.
 */

package com.company;

import java.util.Scanner;

public class data_validation {
    // Division Method
    static void doDivision(){
        Scanner input1 = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);

        //validation number1
        String num1;
        do {
            System.out.println("Enter the FIRST number");
            num1 = input1.nextLine();
        } while (!isNumber(num1));

        //validation number2
        String num2;
        do {
            System.out.println("Enter a valid divisor");
            num2 = input2.nextLine();
        } while (!isGoodDivisor(num2));

        System.out.println("Result of division is: " + Float.parseFloat(num1)/Float.parseFloat(num2));
    }

    //check number Method
    static boolean isNumber(String strNumber){
        boolean isNum =  strNumber.matches("[+-]?\\d*(\\.\\d+)?");
        if (!isNum) System.out.println("Sorry, Invalid number");
        return  isNum;
    }

    //check if number is zero
    static boolean isZero(float fNum){
        //make a zero variable
        Float zero = new Float(0);

        //check if fNum is zero
        if (fNum == zero) {
            return true;
        }
        else {
            return false;
        }
    }

    //check if a string es zero an a number
    static boolean isGoodDivisor(String strNumber){
        if(isNumber(strNumber)){
            if(isZero(Float.parseFloat(strNumber))){
                System.out.println("The Number can not be 0");
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }

    }

    //main method
    public static void main(String[] args) {
        System.out.println("###Welcome to the division calculator - DATA VALIDATION###");
        doDivision();
    }
}
