import java.util.*;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.TreeSet;

public class week02_collections {
    public static void main(String[] args){
        //System.out.println("Hello World");
        //System.out.println(new employee("juan", "perez", 25));

        //------ArrayList
        System.out.println("###ArrayList (allows duplicated items)###");
        ArrayList<employee> employeesList = new ArrayList<>();

        //fill list
        employeesList.add(new employee("juan", "perez", 25));
        employeesList.add(new employee("robert", "trullo", 2));
        employeesList.add(new employee("pepe", "lota", 7));

        //ArrayList allows duplicating elements.
        employeesList.add(new employee("juan", "perez", 25));
        employeesList.add(new employee("juan", "perez", 25));

        //print list of employees
        Iterator<employee> iterator = employeesList.iterator();
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }

        //exception control ArrayList (out of range)
        System.out.println("###ArrayList does not allow access to out-of-range elements###");
        try {
            employeesList.get(200).getYearsExperience();
        }
        catch(Exception e) {
            System.out.println("###index of out range ###");
        }
        System.out.println("");

        //------TreeSet
        System.out.println("###TreeSet (Ordered and No duplicated items allowed.)###");
        TreeSet<employee> employeesTree = new TreeSet<>(new experienceComparator());
        employeesTree.add(new employee("juan", "perez", 25));
        employeesTree.add(new employee("robert", "trullo", 2));
        employeesTree.add(new employee("pepe", "lota", 7));

        //I try to add duplicate elements but the TreeSet does not allow it.
        employeesTree.add(new employee("juan", "perez", 25));
        employeesTree.add(new employee("juan", "perez", 25));

        //exception control TreeSet (TreeSet does not allow null)
        try {
            employeesTree.add(null);
        }
        catch(Exception e) {
            System.out.println("###TreeSet (do not allows null items)###");
        }

        //print all items of the tree
        Iterator<employee> iteratorTree = employeesTree.iterator();
        while (iteratorTree.hasNext())
        {
            System.out.println(iteratorTree.next());
        }
        System.out.println("");

        //------PriorityQueue integers
        PriorityQueue<Integer> priorityQueueNumbers = new PriorityQueue<>();
        priorityQueueNumbers.add(27);
        priorityQueueNumbers.add(12);
        priorityQueueNumbers.add(15);
        priorityQueueNumbers.add(14);

        //natural order is maintained
        System.out.println("Priority Queue content: " + priorityQueueNumbers);

        //get top element without removing it
        System.out.println("Top item (without removing it peek method): " + priorityQueueNumbers.peek());
        System.out.println("Priority Queue content (after peek method): " + priorityQueueNumbers);

        //remove top element
        System.out.println("Get and Remove top item (poll method): " + priorityQueueNumbers.poll());
        System.out.println("Priority Queue content (after remove top element - poll method): " + priorityQueueNumbers);

        //clear all items
        priorityQueueNumbers.clear();

        //I am trying to obtain an element from an empty queue.
        try {
            priorityQueueNumbers.element();
        }
        catch(Exception e) {
            System.out.println("###PriorityQueue is empty -> NoSuchElementException .###");
        }
        System.out.println("");

        //------HashMap
        HashMap<Integer, employee> mapEmployees = new HashMap<Integer, employee>();

        System.out.println("###HashMap (key + Value)###");

        //I use the previous hashtree to fill in the new HashMap
        int i = 0;
        Iterator<employee> iteratorTree2 = employeesTree.iterator();

        while (iteratorTree2.hasNext())
        {
            mapEmployees.put(i, iteratorTree2.next());
            i = i + 1;
        }
        // print key + value
        for (int x : mapEmployees.keySet()) {
            System.out.println("Key: " + x + " value: " + mapEmployees.get(x));
        }
        // print size of the map
        System.out.println("Number of employees on the payroll:" + mapEmployees.size());
        //clear the map
        mapEmployees.clear();


    }
}
