import java.util.*;

public class employee {
    private String name;
    private String lastname;
    private int yearsExperience;

    public employee(String name, String lastname, int yearsExperience){
        this.name = name;
        this.lastname = lastname;
        this. yearsExperience = yearsExperience;
    }

    public String toString(){
        return "Name: " + name + " Last Name: " + lastname + " Years of Experience:" + yearsExperience;
    }

    public int getYearsExperience(){
        return this.yearsExperience;
    }
}

class experienceComparator implements Comparator<employee>
{
    public int compare(employee e1, employee e2)
    {
        return e1.getYearsExperience()-e2.getYearsExperience();
    }
}