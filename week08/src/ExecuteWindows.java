import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteWindows {
    public static void main(String args[])
    {
        //creation of 3 windows
        window w1 = new window("win1", 1, 100, 100);
        window w2 = new window("win2", 2, 50, 50);
        window w3 = new window("win3", 3,150, 150);

        //using executors
        ExecutorService myService = Executors.newFixedThreadPool(3);
        myService.execute(w1);
        myService.execute(w2);
        myService.execute(w3);

        //using threads (another way to do the same)
        //Thread t = new Thread(w3);
        //t.start();

        myService.shutdown();

    }
}
