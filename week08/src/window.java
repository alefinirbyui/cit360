import java.awt.*;
import javax.swing.*;

public class window extends JPanel implements Runnable{

    //a windows with a message, color, and starting point
    String message;
    int startX, startY, color;

    window(String m, int c, int x, int y){
        this.message = m;
        this.startX = x;
        this.startY = y;
        this.color = c;

        //creation of a new windows
        JFrame winFrame = new JFrame();
        winFrame.setSize(200, 200);
        winFrame.setTitle(this.message);

        //set color of the window
        setWindowColor(this.color);

        //add frame and configure values
        winFrame.add(this);
        winFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        winFrame.setVisible(true);

    }

    //paint object into frame
    protected void paintComponent(Graphics g){
        //repaint background
        setWindowColor(this.color);
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial",1,20));
        g.drawString(this.message,this.startX,startY);
        //movement horizontally
        g.drawString(Integer.toString(startX), startX,startX);
    }

    public void setWindowColor(int c) {
        if(this.color == 1){
            this.setBackground(Color.BLUE);
        }
        if(this.color == 2){
            this.setBackground(Color.RED);
        }
        if(this.color != 1 && this.color != 2){
            this.setBackground(Color.GREEN);
        }
    }

    //action
    @Override
    public void run() {
        while(true){
            //reset positions
            if(startX >= 200){
                startX = 0;
            }
            //move right
            startX+=3;
            //clean the window
            repaint();
            try {
                Thread.sleep(100);
            }catch (InterruptedException e) {
                System.err.println(e.toString());
            }
        }
    }
}
