package com.example.week07;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "clearListServlet", value = "/clearListServlet")
public class clearListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out=response.getWriter();
        request.getRequestDispatcher("link.html").include(request, response);

        out.print("<head>\n" +
                "  <link rel=\"stylesheet\" href=\"styles.css\">\n" +
                "</head>");
        out.print("<h1>Clear List</h1>");

        HttpSession session=request.getSession();
        session.invalidate();

        out.print("<h2>The list was successfully cleared</h2>");

        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
