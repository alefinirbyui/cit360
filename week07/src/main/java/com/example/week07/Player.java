package com.example.week07;


public class Player {
    private int id;
    private String name;
    private int age;
    private int yearsPlaying;


    public Player(int id, String name, int age, int yearsPlaying){
        this.id = id;
        this.name = name;
        this.age = age;
        this.yearsPlaying = yearsPlaying;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public int getYearsPlaying() {
        return yearsPlaying;
    }

    @Override
    public String toString() {
        return "{" +
                "id:" + id +
                ", name:'" + name + '\'' +
                ", age:" + age +
                ", yearsPlaying:" + yearsPlaying +
                '}';
    }

    public String toStringWeb() {
        return  "<h3>Id: </h3>" + id +
                "<h3>Name: </h3>" + name +
                "<h3>Age: </h3>" + age +
                "<h3>Years Playing: </h3>" + yearsPlaying  +
                "<hr/>";
    }
}