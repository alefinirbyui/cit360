package com.example.week07;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "viewListWebServlet", value = "/viewListWebServlet")
public class viewListWebServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out=response.getWriter();
        out.print("<head>\n" +
                "  <link rel=\"stylesheet\" href=\"styles.css\">\n" +
                "</head>");
        request.getRequestDispatcher("link.html").include(request, response);
        out.print("<h1>View List</h1>");

        HttpSession session=request.getSession(false);
        if(session!=null){
            //String name=(String)session.getAttribute("name");
            String historic=(String)session.getAttribute("historic");

            //print values
            out.print(historic);
        }
        else{
            out.print("<h2>The list is empty</h2>");
        }
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
