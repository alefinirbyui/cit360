package com.example.week07;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "addPlayerServlet", value = "/addPlayerServlet")
public class addPlayerServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out=response.getWriter();
        request.getRequestDispatcher("link.html").include(request, response);

        out.print("<head>\n" +
                "  <link rel=\"stylesheet\" href=\"styles.css\">\n" +
                "</head>");
        out.print("<h1>Add Player to List</h1>");


        //obtain parameters from form
        String idStr=request.getParameter("id");
        String name=request.getParameter("name");
        String ageStr=request.getParameter("age");
        String yearsPlayingStr=request.getParameter("yearsPlaying");

        //data validation
        boolean isValid = true;
        Integer id = 0;
        Integer age = 0;
        Integer yearsPlaying = 0;
        String message="";

        // check integers values and logic
        try {
            id = Integer.parseInt(idStr);
            age = Integer.parseInt(ageStr);
            yearsPlaying = Integer.parseInt(yearsPlayingStr);
            //check age, yearsPlaying and positives values
            isValid = (age >= 18 && age <= 120) && (yearsPlaying < age) && (id >= 0 && yearsPlaying >= 0 && yearsPlaying <= 120);
        } catch (NumberFormatException e){
            out.print("<h2>The id, age and years playing has to be numeric value</h2><br>");
            isValid = false;
        }

        //creation of new player
        Player p = new Player(id, name, age, yearsPlaying);


        if(isValid){
            out.print("<h2>The Player was successfully added</h2>");
            HttpSession session=request.getSession();
            session.setAttribute("name",name);

            /* save new player on cookie */

            String param = (String) session.getAttribute("historic");

            if (param != null) {
                //not first time - construct de string
                session.setAttribute("historic", param + p.toStringWeb());
            } else {
                //first time
                session.setAttribute("historic", p.toStringWeb());
            }

        }
        else{
            out.print("<h2>The id, age and years experience has to be > 0 and the player has to be an adult</h2><br>");
        }
        out.close();

    }
}
