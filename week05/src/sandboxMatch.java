public class sandboxMatch {
    public static void main(String[] args) {
        System.out.println("== Table tennis players ==");
        Player p1 = new Player(2029,"Ryu",22,12);
        Player p2 = new Player(3432,"Ken",20,8);

        System.out.println(p1.toString());
        System.out.println(p2.toString());

        Player p3 = new Player(2934,"Blanca",55,22);
        Player p4 = new Player(2934,"Bison",11,2);

        System.out.println(p3.toString());
        System.out.println(p4.toString());
        System.out.println();

        System.out.println("== Table tennis teams ==");

        Team t1 = new Team("Boca");
        t1.addPlayer(p1);
        t1.addPlayer(p2);

        Team t2 = new Team("River Plate");
        t2.addPlayer(p3);
        t2.addPlayer(p4);

        System.out.println(t1.toString());
        System.out.println(t2.toString());
    }
}
