import java.util.*;


public class Team {
    private String name;
    //private Player players[];
    private List<Player> players = new ArrayList<Player>();

    public Team(String name, List<Player> players){
        this.name = name;
        this.players = players;
    }

    public Team(String name){
        this.name = name;
        this.players = new ArrayList<Player>();
    }

    public String getName() {
        return name;
    }

    public List<Player> getPlayers() {
        return players;
    }


    /*
    * Preconditions
    * players cannot be null
    * players cannot be the same
    * players cannot be more than 2 players
    * player cannot be < 18 years old
    *
    * */
    public void addPlayer(Player p){
        this.players.add(p);
    }

    public boolean checkAgeTeam(){
        return  (this.players.get(0).getAge() > 18 && this.players.get(1).getAge() > 18);
    }

    @Override
    public String toString() {
        return "Team{" +
                "name='" + name + '\'' +
                ", players=" + players +
                '}';
    }
}
