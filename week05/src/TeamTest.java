import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.Assert.*;

public class TeamTest {

    Player p1, p2, p3, p4;

    @BeforeEach
    public void setUp() throws Exception {
        // creation of 3 players for testing purposes
        p1 = new Player(2029,"Ryu",22,12);
        p2 = new Player(3432,"Ken",20,8);
        p3 = new Player(2934,"Blanca",55,22);
        p4 = new Player(2934,"Bison",11,2);
    }

    @Test
    public void testAddPlayer1() {
        // creation of team1
        Team t1 = new Team("Boca");
        t1.addPlayer(p1);
        t1.addPlayer(p2);

        //--test will not fail--
        //check if the team has 2 members
        assertEquals("The size of the team must be 2: ",t1.getPlayers().size(), 2);


        //--test will fail--
        //add one player forcing errors
        t1.addPlayer(p3);
        t1.toString();
        assertEquals("The size of the team must be 2: ",t1.getPlayers().size(), 2);


    }

    @Test
    public void testAddPlayer2() {
        //check if any player is null
        Team t3 = new Team("Florida");
        t3.addPlayer(p1);
        t3.addPlayer(null);

        //--test will not fail--
        //check player 1
        assertNotNull("The Players cannot be null",t3.getPlayers().get(0));

        //--test will fail--
        //check player2
        assertNotNull("The Players cannot be null",t3.getPlayers().get(1));
    }

    @Test
    public void testAddPlayer3() {
        //check if the player are the same
        //--test will not fail--
        Team t2 = new Team("Nacional");
        t2.addPlayer(p1);
        t2.addPlayer(p3);
        assertNotSame("The player cannot be the same: ", t2.getPlayers().get(0),t2.getPlayers().get(1));

        //--test will fail--
        //check if the player are the same
        Team t3 = new Team("Florida");
        t3.addPlayer(p3);
        t3.addPlayer(p3);
        assertNotSame("The player cannot be the same: ", t3.getPlayers().get(0),t3.getPlayers().get(1));
    }

    @Test
    public void testCheckAgeTeam() {

        //--- test will not fail --
        Team t3 = new Team("Necaksas");
        t3.addPlayer(p1);
        t3.addPlayer(p2);

        //System.out.println(t3);
        assertTrue("The age of one member of the team is wrong", t3.checkAgeTeam());

        //test will fail
        Team t4 = new Team("Florida");
        t4.addPlayer(p1);
        t4.addPlayer(p4);
        assertTrue("The age of one member of the team is wrong", t4.checkAgeTeam());
    }

}