public class Player {
    private int id;
    private String name;
    private int age;
    private int yearsPlaying;


    public Player(int id, String name, int age, int yearsPlaying){
        this.id = id;
        this.name = name;
        this.age = age;
        this.yearsPlaying = yearsPlaying;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public int getYearsPlaying() {
        return yearsPlaying;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", yearsPlaying=" + yearsPlaying +
                '}';
    }
}
