package com.cit360.finalproject;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
    private String message;
    //private Gson gson = new Gson();

    public void init() {
        message = "Hello World!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        branchManager bmanager = new branchManager();
        bmanager.setup();
        //create 2 managers and add this
        Manager m1 = new Manager("Robert", "Trullo", "rtrullo@byui.edu","097085094");
        Manager m2 = new Manager("Reginald", "Picke", "rpicke@byui.edu","097986543");
        Manager m3 = new Manager("Reginald2", "Picke3", "rpicke@byui.edu","097986543");
        Manager m4 = new Manager("Reginald3", "Picke3", "rpicke@byui.edu","097986543");

        // add manager to DB
        bmanager.addManager(m1);
        bmanager.addManager(m2);
        bmanager.addManager(m3);
        bmanager.addManager(m4);

        //create 2 branches
        Branch b1 = new Branch("Tobalaba","Los pinos 2020", "23479887", -33.23333f,-54.383331f, m1);
        Branch b2 = new Branch("La Dehesa","Ohjigins 321", "2343421", -34.86528f,-56.181938f, m2);
        Branch b3 = new Branch("La Dehesa","Ohjigins 321", "2343421", -32,-57.25f, m3);
        Branch b4 = new Branch("La Dehesa","Ohjigins 321", "2343421", -34.337502f,-56.713612f, m4);


        //add branchhes to table
        bmanager.addBranch(b1);
        bmanager.addBranch(b2);
        bmanager.addBranch(b3);
        bmanager.addBranch(b4);

        //String branchesJsonString = "{\"list\":[" + this.gson.toJson(b1) + "," + this.gson.toJson(b2) + "]}";
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");
        //out.print(branchesJsonString);
        //out.print("--------------");
        out.print(bmanager.printAllBranches2());

        out.flush();

    }

    public void destroy() {
    }
}