package com.cit360.finalproject;

import com.google.gson.Gson;

public class Branch {
    private int id;
    private String name;
    private String street;
    private String phone;
    private float latitude;
    private float longitude;
    private Manager manager;

    public Branch(){

    }
    public Branch(String name, String street, String phone, float latitude, float longitude, Manager manager){
        this.name = name;
        this.street = street;
        this.phone = phone;
        this.latitude = latitude;
        this.longitude = longitude;
        this.manager = manager;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getPhone() {
        return phone;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public Manager getManager() {
        return manager;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ",\"name\":\"" + name + '\"' +
                ",\"street\":\"" + street + '\"' +
                ",\"phone\":\"" + phone + '\"' +
                ",\"latitude\":" + latitude +
                ",\"longitude\":" + longitude  +
                "," + manager +
                '}';
    }


    public String btoJSON(){
        Gson gson = new Gson();
        String tmp = gson.toJson(this);
        return tmp;
    }
}
