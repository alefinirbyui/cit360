package com.cit360.finalproject;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "frontend", value = "/frontend")
public class frontend extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        branchManager bmanager = new branchManager();
        bmanager.setup();

        //String branchesJsonString = "{\"list\":[" + this.gson.toJson(b1) + "," + this.gson.toJson(b2) + "]}";
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");

        //print JSON
        out.print(bmanager.printAllBranches2());

        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
