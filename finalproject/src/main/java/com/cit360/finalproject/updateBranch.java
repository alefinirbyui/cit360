package com.cit360.finalproject;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "updateBranch", value = "/updateBranch")
public class updateBranch extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        branchManager bmanager = new branchManager();
        bmanager.setup();

        response.setContentType("text/html");

        PrintWriter out=response.getWriter();
        out.print("<title>View all Branches</title>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "  <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css\">\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js\"></script>\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js\"></script>\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js\"></script>");
        // include menu
        request.getRequestDispatcher("menu.html").include(request, response);
        String sid=request.getParameter("id");
        int id=Integer.parseInt(sid);

        Branch b1 = bmanager.getABranch(id);

        out.print("<div class=\"container\">\n" +
                "    <h2>Update Branch</h2>\n" +
                "    <form action=\"updateBranch\" method=\"post\">\n" +
                "        <div class=\"form-group\">\n" +
                "                <label for=\"bname\">Name:</label>\n" +
                "                <input id=\"bname\" type=\"text\" name=\"bname\" value=\"" + b1.getName() + "\" class=\"form-control\">\n"  +
                "        </div>\n" +
                "        <div class=\"form-group\">\n" +
                "                <label for=\"bstreet\">Street:</label>\n" +
                "                <input id=\"bstreet\" type=\"text\" name=\"bstreet\" value=\"" + b1.getStreet() +"\" class=\"form-control\">\n" +
                "        </div>\n" +
                "        <div class=\"form-group\">\n" +
                "            <label for=\"bphone\">Phone:</label>\n" +
                "            <input id=\"bphone\" type=\"text\" name=\"bphone\" value=\"" + b1.getPhone() + "\" class=\"form-control\">\n" +
                "        </div>\n" +
                "        <div class=\"form-group\">\n" +
                "                <label for=\"blatitude\">Latitude:</label>\n" +
                "                <input id=\"blatitude\" type=\"text\" name=\"blatitude\" value=\"" + b1.getLatitude() + "\" class=\"form-control\">\n" +
                "        </div>\n" +
                "        <div class=\"form-group\">\n" +
                "                <label for=\"blongitude\">Longitude:</label>\n" +
                "                <input id=\"blongitude\" type=\"text\" name=\"blongitude\"  value=\"" + b1.getLongitude() + "\" class=\"form-control\">\n" +
                "        </div>\n" +
                "        <div class=\"form-group\">\n" +
                "                <label for=\"mname\">Manager Name</label>\n" +
                "                <input id=\"mname\" type=\"text\" name=\"mname\"  value=\"" + b1.getManager().getName() + "\" class=\"form-control\">\n" +
                "        </div>\n" +
                "        <div class=\"form-group\">\n" +
                "                <label for=\"mlastname\">Manager Last Name</label>\n" +
                "                <input id=\"mlastname\" type=\"text\" name=\"mlastname\" value=\"" + b1.getManager().getLastname() + "\" class=\"form-control\">\n" +
                "        </div>\n" +
                "        <div class=\"form-group\">\n" +
                "                <label for=\"memail\">Manager mail:</label>\n" +
                "                <input id=\"memail\" type=\"text\" name=\"memail\"  value=\"" + b1.getManager().getEmail() + "\"  class=\"form-control\">\n" +
                "        </div>\n" +
                "        <div class=\"form-group\">\n" +
                "                <label for=\"mmobile\">Manger Mobile:</label>\n" +
                "                <input type=\"text\" id=\"mmobile\" name=\"mmobile\"  value=\"" + b1.getManager().getMobile() + "\" class=\"form-control\">\n" +
                "        </div>\n" +
                "          <input type=\"hidden\" name=\"idBranch\" value="+ id +">" +
                "          <input type=\"hidden\" name=\"idManager\" value="+ b1.getManager().getId() +">" +
                "        <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n" +
                "    </form>\n" +
                "</div>");
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out=response.getWriter();
        out.print("<title>View all Branches</title>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "  <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css\">\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js\"></script>\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js\"></script>\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js\"></script>");
        // include menu
        request.getRequestDispatcher("menu.html").include(request, response);

        out.print("<head>\n" +
                "  <link rel=\"stylesheet\" href=\"styles.css\">\n" +
                "</head>");


        //obtain parameters from form
        //String idStr=request.getParameter("id");
        String bname=request.getParameter("bname");
        String bstreet=request.getParameter("bstreet");
        String bphone=request.getParameter("bphone");
        String blatitude=request.getParameter("blatitude");
        String blongitude=request.getParameter("blongitude");
        String mname=request.getParameter("mname");
        String mlastname=request.getParameter("mlastname");
        String memail=request.getParameter("memail");
        String mmobile=request.getParameter("mmobile");
        String idBranch=request.getParameter("idBranch");
        String idManager=request.getParameter("idManager");

        // get manager id and branch id
        int idB = Integer.parseInt(idBranch);
        int idM = Integer.parseInt(idManager);

        // data validation
        boolean isValid = true;
        String message="";
        float latitude = -200f;
        float longitude = -200f;
        final float LATMAX = 90.0000000f;
        final float LATMIN = -90.0000000f;
        final float LONGMAX = 180.0000000f;
        final float LONGMIN = -180.0000000f;


        // check values and logic
        try {
            latitude = Float.parseFloat(blatitude);
            longitude = Float.parseFloat(blongitude);

            //check min and max values and not null values
            isValid = (latitude >= LATMIN && latitude <= LATMAX) && (longitude >= LONGMIN && longitude <= LONGMAX) && (!bname.isEmpty() && !bstreet.isEmpty() && !bphone.isEmpty() && !mname.isEmpty() && !mlastname.isEmpty() && !memail.isEmpty() && !mmobile.isEmpty());

        } catch (NumberFormatException e){
            //out.print("<h1>Please insert valid information</h1><br>");
            out.print("<div class=\"alert alert-danger\">\n" +
                    "  <strong>Please insert valid information!!!\n" +
                    "</div>");
            out.close();
            isValid = false;
        }

        if(isValid) {
            //creation of new branch
            branchManager bmanager = new branchManager();
            bmanager.setup();
            //create 2 managers and add this
            Manager m1 = new Manager(mname, mlastname, memail, mmobile);
            m1.setId(idM);

            // add manager to DB
            bmanager.updateManager(m1);

            //create branch
            Branch b1 = new Branch(bname, bstreet, bphone, latitude, longitude, m1);
            b1.setId(idB);

            //add branches to table
            bmanager.updateBranch(b1);
            //out.print("<h2>Branch was successfully </h2><br>");
            out.print("<div class=\"alert alert-success\">\n" +
                    "  <strong>Branch was successfully Updated!!!\n" +
                    "</div>");
        }else {
            out.print("<div class=\"alert alert-danger\">\n" +
                    "  <strong>Please insert valid information!!!\n" +
                    "</div>");
        }

        //response.sendRedirect("readBranches");
        out.close();

    }
}
