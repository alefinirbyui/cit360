package com.cit360.finalproject;

import com.cit360.finalproject.Branch;
import com.cit360.finalproject.Manager;
import com.cit360.finalproject.branchManager;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "createBranch", value = "/createBranch")
public class createBranch extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out=response.getWriter();
        out.print("<title>View all Branches</title>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "  <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css\">\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js\"></script>\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js\"></script>\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js\"></script>");
        // include menu
        request.getRequestDispatcher("menu.html").include(request, response);

        out.print("<head>\n" +
                "  <link rel=\"stylesheet\" href=\"styles.css\">\n" +
                "</head>");


        //obtain parameters from form
        //String idStr=request.getParameter("id");
        String bname=request.getParameter("bname");
        String bstreet=request.getParameter("bstreet");
        String bphone=request.getParameter("bphone");
        String blatitude=request.getParameter("blatitude");
        String blongitude=request.getParameter("blongitude");
        String mname=request.getParameter("mname");
        String mlastname=request.getParameter("mlastname");
        String memail=request.getParameter("memail");
        String mmobile=request.getParameter("mmobile");

        // data validation
        boolean isValid = true;
        String message="";
        float latitude = -200f;
        float longitude = -200f;
        final float LATMAX = 90.0000000f;
        final float LATMIN = -90.0000000f;
        final float LONGMAX = 180.0000000f;
        final float LONGMIN = -180.0000000f;


        // check values and logic
        try {
            latitude = Float.parseFloat(blatitude);
            longitude = Float.parseFloat(blongitude);

            //check min and max values and not null values
            isValid = (latitude >= LATMIN && latitude <= LATMAX) && (longitude >= LONGMIN && longitude <= LONGMAX) && (!bname.isEmpty() && !bstreet.isEmpty() && !bphone.isEmpty() && !mname.isEmpty() && !mlastname.isEmpty() && !memail.isEmpty() && !mmobile.isEmpty());

        } catch (NumberFormatException e){
            //out.print("<h1>Please insert valid information</h1><br>");
            out.print("<div class=\"alert alert-danger\">\n" +
                    "  <strong>Please insert valid information!!!\n" +
                    "</div>");
            out.close();
            isValid = false;
        }

        if(isValid) {
            //creation of new branch
            branchManager bmanager = new branchManager();
            bmanager.setup();
            //create 2 managers and add this
            Manager m1 = new Manager(mname, mlastname, memail, mmobile);

            // add manager to DB
            bmanager.addManager(m1);
            //create 2 branches
            Branch b1 = new Branch(bname, bstreet, bphone, latitude, longitude, m1);

            //add branches to table
            bmanager.addBranch(b1);
            //out.print("<h2>Branch was successfully Added</h2><br>");
            out.print("<div class=\"alert alert-success\">\n" +
                    "  <strong>Branch was successfully Added!!!\n" +
                    "</div>");
        }else {
            out.print("<div class=\"alert alert-danger\">\n" +
                    "  <strong>Please insert valid information!!!\n" +
                    "</div>");
        }

        //response.sendRedirect("readBranches");
        out.close();


    }
}
