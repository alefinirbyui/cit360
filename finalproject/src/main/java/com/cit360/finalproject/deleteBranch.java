package com.cit360.finalproject;

import com.cit360.finalproject.Manager;
import com.cit360.finalproject.branchManager;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "deleteBranch", value = "/deleteBranch")
public class deleteBranch extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        branchManager bmanager = new branchManager();
        bmanager.setup();

        String sid=request.getParameter("id");
        int id=Integer.parseInt(sid);

        Manager m1 = bmanager.getABranch(id).getManager();

        bmanager.dropBranch(bmanager.getABranch(id));

        bmanager.deleteManager(m1);

        response.setContentType("text/html");
        PrintWriter out=response.getWriter();
        out.print("<title>View all Branches</title>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "  <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css\">\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js\"></script>\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js\"></script>\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js\"></script>");
        // include menu
        request.getRequestDispatcher("menu.html").include(request, response);
        //out.print("<h2>Branch was successfully deleted</h2><br>");
        out.print("<div class=\"alert alert-success\">\n" +
                "  <strong>Branch was successfully deleted!!!\n" +
                "</div>");
        //response.sendRedirect("readBranches");




    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
