package com.cit360.finalproject;

public class Manager {
    private int id;
    private String name;
    private String lastname;
    private String email;
    private String mobile;

    public Manager(){

    }
    public Manager(String name, String lastname, String email, String mobile){
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.mobile = mobile;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getLastname() {
        return lastname;
    }
    public String getEmail() {
        return email;
    }
    public String getMobile() {
        return mobile;
    }

    @Override
    public String toString() {
        return "\"manager\":{" +
                "\"id\":" + id +
                ",\"name\":\"" + name + '\"' +
                ",\"lastname\":\"" + lastname + '\"' +
                ",\"email\":\"" + email + '\"' +
                ",\"mobile\":\"" + mobile + '\"' +
                '}';
    }
}
