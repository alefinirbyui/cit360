package com.cit360.finalproject;

import com.cit360.finalproject.Branch;
import com.cit360.finalproject.branchManager;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "readBranches", value = "/readBranches")
public class readBranches extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        branchManager bmanager = new branchManager();
        bmanager.setup();

        response.setContentType("text/html");
        PrintWriter out=response.getWriter();
        out.print("<title>View all Branches</title>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "  <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css\">\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js\"></script>\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js\"></script>\n" +
                "  <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js\"></script>");
        // include menu
        request.getRequestDispatcher("menu.html").include(request, response);


        List<Branch> list = bmanager.gettAllBranches();

        //print table
        out.print("<div class=\"container\">");
        out.println("<h1>Branches List</h1>");
        out.print("<table class=\"table table-hover\">");

        // print head of the table
        out.print("<tr><th>Id</th><th>Name</th><th>Street</th><th>Latitude</th><th>Longitude</th><th>Manager</th><th>Email</th><th>Mobile</th><th>Edit</th><th>Delete</th></tr>");
        for(Branch b:list){
            out.print("<tr><td>"+b.getId()+"</td><td>"+b.getName()+"</td><td>"+b.getStreet()+"</td><td>"+b.getLatitude()+"</td><td>"+b.getLongitude()+"</td><td>"+b.getManager().getName()+ " " + b.getManager().getLastname() +"</td><td>"+b.getManager().getEmail() + "</td><td>" + b.getManager().getMobile() +"</td><td><a href='updateBranch?id="+b.getId()+"'>edit</a></td> <td><a href='deleteBranch?id="+b.getId()+"'>delete</a></td></tr>");
        }

        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
