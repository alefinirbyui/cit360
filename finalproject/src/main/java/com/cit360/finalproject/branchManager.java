package com.cit360.finalproject;

//import com.google.gson.Gson;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

public class branchManager {
    protected SessionFactory sessionFactory;
    //setup connection to mysql
    public void setup() {
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() //settings from hibernate.cfg.xml
                .build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception ex) {
            StandardServiceRegistryBuilder.destroy(registry);
            System.out.println("The connection could not be made..");
        }
    }

    protected void exit() {
        try{
            sessionFactory.close();
        }catch (Exception e) {
            System.out.println("The Session cannot be closed");
            e.printStackTrace();
        }
    }
    /*----------------Branches-----------------*/
    //create a branch in database
    public void addBranch(Branch b1) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            //save branch
            session.save(b1);
            session.getTransaction().commit();
            session.close();
        }catch (Exception e) {
            System.out.println("The Branch cannot be added");
            e.printStackTrace();
        }
    }
    //get a Branch and print it
    public void readBranch(int branchId) {
        try(Session session = sessionFactory.openSession()){
            Branch b1 = session.get(Branch.class, branchId);
            if(b1 != null) {
                System.out.println(b1.toString());
                //session.delete(b1);
            }
            session.close();
        }catch (Exception e) {
            System.out.println("Branch information could not be obtained.");
            e.printStackTrace();
        }
    }
    public Branch getABranch(int branchId) {
        try(Session session = sessionFactory.openSession()){
            Branch b1 = session.get(Branch.class, branchId);
            session.close();
            return b1;
        }catch (Exception e) {
            System.out.println("Branch information could not be obtained.");
            e.printStackTrace();
        }
        return null;
    }
    // return the string and do not print it
    public String readBranch2(int branchId) {
        String val = null;
        try(Session session = sessionFactory.openSession()){
            Branch b1 = session.get(Branch.class, branchId);
            if(b1 != null) {
                val = b1.toString();
            }
            session.close();
        }catch (Exception e) {
            System.out.println("Branch information could not be obtained.");
            e.printStackTrace();
        }
        return  val;
    }

    //update a Branch
    protected void updateBranch(Branch b1) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            //update player
            session.update(b1);
            session.getTransaction().commit();
            session.close();
        }catch (Exception e) {
            System.out.println("Branch information could not be updated.");
            e.printStackTrace();
        }
    }
    //update a Branch
    public void dropBranch(Branch b1) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            //update player
            session.delete(b1);
            session.getTransaction().commit();
            session.close();
        }catch (Exception e) {
            System.out.println("Branch information could not be updated.");
            e.printStackTrace();
        }
    }

    public void deleteOneBranch(int branchId) {
        try(Session session = sessionFactory.openSession()){
            Branch b1 = session.get(Branch.class, branchId);
            if(b1 != null) {
                System.out.println(b1.toString());
                session.delete(b1);
            }
            session.close();
            //deleteManger(b1.getManager());
        }catch (Exception e) {
            System.out.println("Branch could not be deleted.");
            e.printStackTrace();
        }
    }


	//print branch table (maybe we had to execute a different query)
	protected void printAllBranches(){
		try(Session session = sessionFactory.openSession()){
			session.beginTransaction();
			List<Branch> branches = session.createQuery("from Branch", Branch.class).list();
			branches.forEach(b -> System.out.println(b.toString()));
			session.getTransaction().commit();
			session.close();
		}catch (Exception e) {
			System.out.println("Branches information could not be obtained.");
			e.printStackTrace();
		}
	}
    public List<Branch> gettAllBranches(){
        List<Branch> branches = null;
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            branches = session.createQuery("from Branch", Branch.class).list();
            //branches.forEach(b -> System.out.println(b.toString()));
            session.getTransaction().commit();
            session.close();
        }catch (Exception e) {
            System.out.println("Branches information could not be obtained.");
            e.printStackTrace();
        }
        return branches;
    }
    protected String printAllBranches2(){
        String val = "x";
        String tmp = "";

        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            List<Branch> branches = session.createQuery("from Branch", Branch.class).list();
            for (Branch b : branches)
            {
                tmp  += b.toString() + ",";
            }
            //drop last ,
            val = "{\"list\":[" + tmp.substring(0, tmp.length()-1) + "]}";
            session.getTransaction().commit();
            session.close();
        }catch (Exception e) {
            System.out.println("Branches information could not be obtained.");
            e.printStackTrace();
        }
        return val;
    }
/*----------------Managers-----------------*/
public void addManager(Manager m1) {
    try(Session session = sessionFactory.openSession()){
        session.beginTransaction();
        //save branch
        session.save(m1);
        session.getTransaction().commit();
        session.close();
    }catch (Exception e) {
        System.out.println("The Branch cannot be added");
        e.printStackTrace();
    }
}
    //get a Branch and print it
    protected void readManager(int managerId) {
        try(Session session = sessionFactory.openSession()){
            Manager m1 = session.get(Manager.class, managerId);
            if(m1 != null) {
                System.out.println(m1.toString());
            }
            session.close();
        }catch (Exception e) {
            System.out.println("Manager information could not be obtained.");
            e.printStackTrace();
        }
    }
    // return the string and do not print it
    protected String readMananger2(int managerId) {
        String val = null;
        try(Session session = sessionFactory.openSession()){
            Manager m1 = session.get(Manager.class,managerId);
            if(m1 != null) {
                val = m1.toString();
            }
            session.close();
        }catch (Exception e) {
            System.out.println("Manager information could not be obtained.");
            e.printStackTrace();
        }
        return  val;
    }

    //update a Branch
    protected void updateManager(Manager m1) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            //update player
            session.update(m1);
            session.getTransaction().commit();
            session.close();
        }catch (Exception e) {
            System.out.println("Manager information could not be updated.");
            e.printStackTrace();
        }
    }

    //delete a manager
    public void deleteManager(Manager m1) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            //delete player
            session.delete(m1);
            session.getTransaction().commit();
            session.close();
        }catch (Exception e) {
            System.out.println("Manager information could not be deleted.");
            e.printStackTrace();
        }
    }

    //print branch table (maybe we had to execute a different query)
    protected void printAllMangers(){
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            List<Manager> managers = session.createQuery("from Manager", Manager.class).list();
            managers.forEach(b -> System.out.println(b.toString()));
            session.getTransaction().commit();
            session.close();
        }catch (Exception e) {
            System.out.println("Managers information could not be obtained.");
            e.printStackTrace();
        }
    }
/*----------------------------------------*/
    public static void main(String[] args) {
      branchManager bmanager = new branchManager();
        bmanager.setup();

        //create 2 managers and add this
        Manager m1 = new Manager("Robert", "Trullo", "rtrullo@byui.edu","097085094");
        Manager m2 = new Manager("Reginald", "Picke", "rpicke@byui.edu","097986543");

        // add manager to DB
        bmanager.addManager(m1);
        bmanager.addManager(m2);

        //create 2 branches
        Branch b1 = new Branch("Tobalaba","Los pinos 2020", "23479887", 10.5876f,10.2877f, m1);
        Branch b2 = new Branch("La Dehesa","Ohjigins 321", "2343421", 10.798f,10.9f, m2);

        //add branchhes to table
        bmanager.addBranch(b1);
        bmanager.addBranch(b2);

        //print branch 1 info
        System.out.println("---------------------");
        bmanager.readBranch(b1.getId());
        System.out.println("---------------------");

        //print all branches
        bmanager.printAllBranches();
        bmanager.printAllMangers();

        Manager m4 = bmanager.getABranch(1).getManager();
        Manager m5 = bmanager.getABranch(2).getManager();

        System.out.println(m4.toString());
        //m5.toString();

        bmanager.dropBranch(bmanager.getABranch(1));
        bmanager.dropBranch(bmanager.getABranch(2));

        bmanager.deleteManager(m4);
        bmanager.deleteManager(m5);
        //print all players
        //manager.printAllPlayers();

        //delete team1
       // manager.deleteTeam(t1);



        //exit and delete all tables
        //manager.exit();
    }

}
