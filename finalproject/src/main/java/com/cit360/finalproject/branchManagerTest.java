package com.cit360.finalproject;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class branchManagerTest {
    Manager m1;
    Branch b1;
    branchManager bmanager = new branchManager();

    @BeforeEach
    public void setUp() throws Exception {
        m1 = new Manager("Frank", "Hurtado", "fhurtado@byui.edu","099086084");
        b1 = new Branch("Colombia","Los bartulos 2023", "23479897", -32.23333f,-52.383331f, m1);
    }

    @Test
    public void testAddManager() {
        // adding m1
        b1.setManager(m1);
        //--test will not fail--
        //check if the manager was properly added
        assertEquals("099086084",b1.getManager().getMobile(), "The mobile of the Manager must be 099086084:");

    }

    @Test
    public void testAddBranch() {
        //--test will not fail--
        //check manager m1
        bmanager.setup();
        // add manager to DB
        bmanager.addManager(m1);
        //add manager to branch
        b1.setManager(m1);
        //add branches to db
        bmanager.addBranch(b1);
        Manager m2 = bmanager.getABranch(b1.getId()).getManager();
        assertNotNull(m2);
    }

    @Test
    public void testDropBranch() {
        bmanager.setup();
        //--test will not fail--
        int id = b1.getId();
        Manager m2 = bmanager.getABranch(id).getManager();
        bmanager.dropBranch(bmanager.getABranch(id));
        bmanager.deleteManager(m2);
        assertNull(bmanager.getABranch(id));
    }
}