import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.nio.charset.StandardCharsets;

import com.sun.net.httpserver.*;

import java.net.BindException;
import java.net.InetSocketAddress;
import java.io.*;

public class server {
    //object to json
    public static String bookToJson(book bookTemp){
        ObjectMapper mapper = new ObjectMapper();
        String strTemp= "";
        //try conversion
        try {
            strTemp = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(bookTemp);
        }catch (JsonProcessingException e){
            System.err.println(e.toString());
        }

        return strTemp;
    }

    // handler for request
    static class pageHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange x) throws IOException {

            book book1 = new book(1, "Ender's Game");

            //convert book1 object into json
            String response = bookToJson(book1);

            x.getResponseHeaders().set("Content-Type", "application/json");
           // x.sendResponseHeaders(200, response.length());
            x.sendResponseHeaders(200, response.getBytes(StandardCharsets.UTF_8).length);
            OutputStream os = x.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    //main
    public static void main(String[] args) throws IOException {
        //create http server and check address/port
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
            server.createContext("/", new pageHandler());
            server.setExecutor(null); // creates a default executor
            server.start();
            System.out.println("-- Server is running on http://localhost:8080 --");
        }catch (BindException e){
            System.out.println("Port or Address already in use");
        }
    }
}
