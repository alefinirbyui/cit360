import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.*;
import java.io.*;
import java.util.*;

public class client {

    //json to object
    public static book jsonToBook(String strBook){
        ObjectMapper mapper = new ObjectMapper();
        book bookTemp = null;

        //try conversion
        try {
            bookTemp = mapper.readValue(strBook, book.class);
        }catch (JsonProcessingException e){
            System.out.println("Error during object conversion");
        }
        return bookTemp;
    }

    //obtain info from remote origin
    public static String getHttpData(String strUrl) {
        try{
            URL url = new URL(strUrl);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader( new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while((line = reader.readLine())!= null){
                stringBuilder.append(line + "\n");
            }

            return stringBuilder.toString();
        }
        catch (ConnectException e) {
            //offline server
            System.out.println("Sorry, the Server is offline");
        }
        catch (IOException e){
            System.err.println(e.toString());
        }

        return "Error";
    }

    public static Map getHttpHeaders(String strUrl){
        try {
            URL url = new URL(strUrl);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            return http.getHeaderFields();
        }catch (IOException e){
            System.err.println(e.toString());
        }
        return null;
    }

    public static void main(String[] args) {

        // call to server class and obtain a json and convert it into object and print it
        System.out.println("-- Server Content from http://localhost:8080/ --");

        String json= getHttpData("http://localhost:8080/");

        //check conversion error
        if(json !="Error"){
            System.out.println(jsonToBook(json));
        };

        System.out.println("-- Server HEADERS of http://localhost:8080/ --");

        Map<Integer, List<String>> m = getHttpHeaders("http://localhost:8080/");

        //server offline no data
        if (m.isEmpty()){
            System.out.println("Sorry, HEADERS are not available.");
        }else{
            for (Map.Entry<Integer, List<String>> entry : m.entrySet()) {
                System.out.println("Key: " + entry.getKey() + " Value: " + entry.getValue());
            }
        }
    }
}
