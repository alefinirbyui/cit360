public class book {
    private int id;
    private String title;

    //setters
    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    //getters
    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public book(int id, String title){
        this.id = id;
        this.title = title;
    }

    public book(){
        this.id = -1;
        this.title = "";
    }

    public String toString(){
        return "Id: " + id + " Title: " + title;
    }

}
